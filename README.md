# journal - the journalling program for the shell

# install

Two modes:
- developer, where scripts are used from the source dir
- user, where scripts are installed to /usr/local/bin

# todo
- variable/changing files should be OUTSIDE of your git repo
- split out the bin dir for journal from the actual content
- ~/journal should ONLY be content, the bin/scripts should either be just "installed" as journal-* or add ~/src/journal/bin to PATH
- other bits could be added to a profile.d/journal.sh file! :O
- make it easy to adjust start/stop times of a particular instance in the jlogs (journal logs)
- journal-grep -rin (grep through all journal entries)
- history-grep (hg? that's a source code control software)
- cmdlog-grep (cg) -rin recurse (multiple days)
- journal-time-report <from date/time> <to date/time> <grep> to make a detailed report and summary for that time period, say for `support-4705` :)

```
craig@other:~/journal/jlogs$ ed 2020-08-13.md
331
1zn
1       2020-08-13.08:35:24 (null) /home/craig STOP (null) at Thu 13 Aug 2020 08:35:24 AM CDT
2       2020-08-13.08:35:24 cfengine-review-planning /home/craig START cfengine-review-planning at Thu 13 Aug 2020 08:35:24 AM CDT
3       2020-08-13.08:35:27 cfengine-review-planning /home/craig STOP cfengine-review-planning at Thu 13 Aug 2020 08:35:27 AM CDT
2
2020-08-13.08:35:24 cfengine-review-planning /home/craig START cfengine-review-planning at Thu 13 Aug 2020 08:35:24 AM CDT
s.8:35:24.8:00:00.g
wq
331
craig@other:~/journal/jlogs$ jt
2127 cfengine-review-planning
```


