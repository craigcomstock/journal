function yesterday()
{
  # TODO make format an arg?
  date -d "@$(( $(date +%s) - 60*60*24))" +%F
}

function today()
{
  date +%F
}
