echo "START of profile.d/journal.sh"
# TODO don't do this except in "developer" mode?
[ -r $HOME/src/journal/bin ] && export PATH=$HOME/src/journal/bin:$PATH

if [ -f "$HOME/.journalrc" ]; then
  source $HOME/.journalrc
fi
export CMDLOGS=$HOME/Logs/$(hostname)
export JOURNAL_CONTEXT_PATH=$HOME/journal/.context
[ -w $JOURNAL_CONTEXT_PATH ] || echo "null" > $JOURNAL_CONTEXT_PATH
export JOURNAL_DATA=$HOME/journal
mkdir -p $CMDLOGS
#export PROMPT_COMMAND='if [ "$(id -u)" -ne 0 ]; then echo "$(date +%F-%T) $hostname $(cat $JOURNAL_CONTEXT_PATH) $(pwd) $(history 1)" >> $CMDLOGS/$(date +%F).log; fi'
export PROMPT_COMMAND='if [ "$(id -u)" -ne 0 ]; then echo "$(date +%F-%T) $(cat $JOURNAL_CONTEXT_PATH) $(pwd) $(history 1)" >> $CMDLOGS/$(date +%F).log; fi'

_start()
{
  local cur contexts
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  contexts=$(ls ~/journal/clogs/* | sed -e 's/.md//' -e 's,.*/,,')
  COMPREPLY=( $(compgen -W "${contexts}" -- ${cur}) )
  return 0
}
complete -F _start start
echo "END of profile.d/journal.sh"
